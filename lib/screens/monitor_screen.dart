import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:provider/provider.dart';
import '../state/mqtt_instance.dart';

///screen này dùng để hiển thị dữ liệu sensor nhận được qua MQTT
class MonitorScreen extends StatelessWidget {
  const MonitorScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('rebuild monitor screen tree');
    //Size size = MediaQuery.of(context).size;
    final _mqttInstance = Provider.of<MqttInstance>(context);
    return Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <
        Widget>[
      Expanded(
        child: SfRadialGauge(
          title: const GaugeTitle(text: 'Speed Value'),
          enableLoadingAnimation: true,
          animationDuration: 4500,
          axes: <RadialAxis>[
            RadialAxis(minimum: 0, maximum: 255, pointers: <GaugePointer>[
              NeedlePointer(value: _mqttInstance.speed, enableAnimation: true)
            ], ranges: <GaugeRange>[
              GaugeRange(startValue: 0, endValue: 50, color: Colors.green),
              GaugeRange(startValue: 50, endValue: 150, color: Colors.orange),
              GaugeRange(startValue: 150, endValue: 255, color: Colors.red),
            ], annotations: const <GaugeAnnotation>[
              GaugeAnnotation(
                  widget: Text('Unit'), positionFactor: 0.8, angle: 90),
            ]),
          ],
        ),
      )
    ]);
  }
}
