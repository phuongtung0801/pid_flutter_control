class User {
  String light;
  String temperature;
  String humidity;
  User(this.light, this.temperature, this.humidity);
  User.fromJson(Map<String, dynamic> json)
      : light = json['light'],
        temperature = json['temperature'],
        humidity = json['humidity'];
  Map<String, dynamic> toJson() => {
        'light': light,
        'temperature': temperature,
        'humidity': humidity,
      };
}
